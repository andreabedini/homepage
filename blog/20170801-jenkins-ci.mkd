---
author: Ben Gamari
title: "Meet Jenkins: GHC's new CI and build infrastructure"
date: 2017-08-01
tags: ci testing infrastructure
---

While Phabricator is generally well-liked among GHC developers, GHC's
interaction with Harbormaster, Phabricator's continuous integration
component, has been less than rosy. The problem is in large part a mismatch
between Harbormaster's design assumptions and GHC's needs, but it's also
in part attributable to the somewhat half-finished state in which
Harbormaster seems to linger. Regardless, we won't go into detail here;
these issues are well covered [elsewhere](https://gitlab.haskell.org/ghc/ghc/wikis/ticket:13716).

Suffice it to say that, after having looked at a number of alternatives to
Harbormaster (including [buildbot](https://buildbot.net/), GitLab's
[Pipelines](https://gitlab.com/), [Concourse](https://concourse.ci/),
and home-grown solutions), Jenkins seems to be the best option at
the moment. Of course, this is not to say that it is perfect; as we have
learned over the last few months it is very far from perfect. However,
it has the maturity and user-base to be almost-certainly able to handle
what we need of it on the platforms that we care about.

See the Trac ticket #13716

Let's see what we get out of this new bit of infrastructure:

### Pre-merge testing

Currently there are two ways that code ends up in `master`,

 * a Differential is opened, built with Harbormaster, and eventually
   landed (hopefully, but not always, after Harbormaster successfully finishes)
 * someone pushes commits directly

Bad commits routinely end up merged via both channels. This means that
authors of patches failing CI often need to consider whether *their*
patch is incorrect or whether they rather simply had the misfortune of
basing their patch on a bad commit. Even worse, if the commit isn't
quickly reverted or fixed GHC will end up with a hole in its commit
history where neither bisection nor performance tracking will be possible.
For these reasons, we want to catch these commits before they make it
into `master`.

To accomplish this we have developed some
[tooling](https://github.com/bgamari/ghc-auto-push) to run CI on
commits *before* they are finally merged to `master`. By making CI the
only path patches can take to get to `master`, improve our changes of
rejecting bad patches before they turn the tree red.


### Automation of the release builds

Since the 7.10.3 release we have been gradually working towards
automating GHC's release process. Thanks to this work, today a single
person can build binary distributions for all seven tier-1
configurations in approximately a day, most of which is spent simply
waiting. This has allowed us to take responsibility (starting in
8.2.1) for the OpenBSD, FreeBSD, ARMv7 and AArch64 builds in addition to
the traditional tier-1 platforms, allowing us to eliminate the week-long
wait between source distribution availability and the binary
distribution announcement previously needed for correspondence with
binary build contributors..

However, we are far from done: our new Jenkins-based build infrastructure
(see #13716) will allow us to produce binary distributions directly from CI,
reducing the cost of producing release builds to nearly nothing.


### Testing of GHC against user packages

While GHC is already tested against Hackage and Stackage prior to release
candidate availability, these builds have been of limited use as
packages low on the dependency tree (think `hashable` and `lens`)
often don't build prior to the first release candidate. While we do our
best to fix these packages up, the sheer number of them makes
this a losing battle for a small team such as GHC's.

Having the ability to cheaply produce binary distributions means that we
can produce and validate nightly snapshot releases. This gives users a
convenient way to test pre-release compilers and fix their libraries
accordingly. We hope this will spread the maintenance effort across a
larger fraction of the Haskell community and over a longer period of
time, meaning there will be less to do at release time and consequently
pre-release Stackage builds will be more fruitful.

Once the Jenkins infrastructure is stable, we can consider introducing
nightly builds of user packages as well. While building a large
population such as Stackage would likely not be productive, working with
a smaller sample of popular, low-dependency-count packages would be
quite possible. For testing against larger package repositories, leaning on a dedicated tool such as the
[Hackage Matrix Builder](https://matrix.hackage.haskell.org/) will
likely be a more productive path.

### Expanded platform coverage of CI

While GHC targets a wide variety of architectures and operating systems
(and don't forget cross-compilation targets),
by far the majority of developers use Linux, Darwin, or Windows on
amd64. This means that breakage often only comes to light long after the
culpable patch was merged.

Of course, GHC, being a project with modest financial resources, can't
test each commit on every supported platform. We can, however, shrink
the time between a bad commit being merged and the breakage being found
by testing these "unusual" platforms on a regular (e.g. nightly) basis.

By catching regressions early, we hope to reduce the amount of time
spent bisecting and fixing bugs around release time.

### Tracking core libraries

Keeping GHC's core library dependencies (e.g. `directory`, `process`) up-to-date with their respective upstreams
is important to ensure that tools that link against the `ghc` library (e.g. `ghc-mod`) can build easily.
However, it also requires that we work with nearly a dozen upstream
maintainers at various points in their own release cycles to arrange
that releases are made prior to the GHC release. Moreover, there is
inevitably a fair amount of work propagating verion bounds changes down
the dependency tree. While this work takes relatively little effort in
terms of man-hours,

Jenkins can help us here by allowing us to automate integration testing
of upstream libraries, catching bounds issues and other compatibility
issues well before they are in the critical path of the release.

### Improved debugging tools

One of the most useful ways to track down a bugs in GHC is bisection.
This is especially true for regressions found in release candidates,
where you have at most a few thousand commits to bisect through.
Nevertheless, GHC builds are long and developer time scarce so this
approach isn't used as often as it could be.

Having an archive of nightly GHC builds will free the developer from
having to build dozens of compilers during bisection, making the process
a significantly more enjoyable experience than it is today. This will
allow us to solve more bugs in less time and with far fewer grey hairs.

## Status of Jenkins effort

The Jenkins CI overhaul has been an on-going project throughout the
spring and summer and is nearing completion. The Jenkins configuration
can be seen in the `wip/jenkins` branch on `git.haskell.org`
([gitweb](https://git.haskell.org/ghc.git/shortlog/refs/heads/wip/jenkins)). At the moment the prototype is running on a few private machines but we will be setting up a publicly accessible test instance in the coming
weeks. Jenkins will likely coexist with our current Harbormaster
infrastructure for a month or so while we validate that things are
stable.
